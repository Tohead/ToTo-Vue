package cn.zhuxuehui.system.service;
import cn.zhuxuehui.system.domain.SysConfig;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 22:54
 * @description 参数配置服务层
 */

public interface ISysConfigService {

    /**
     * 查询参数配置信息
     * @param configId  参数配置Id
     * @return  参数配置信息
     */
    public SysConfig selectConfigById(Long configId);
}
