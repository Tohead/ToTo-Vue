package cn.zhuxuehui.system.service.impl;

import cn.zhuxuehui.common.annotation.DataSource;
import cn.zhuxuehui.common.enums.DataSourceType;
import cn.zhuxuehui.system.domain.SysConfig;
import cn.zhuxuehui.system.mapper.SysConfigMapper;
import cn.zhuxuehui.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 22:56
 * @description
 */
@Service
public class SysConfigServiceImpl implements ISysConfigService {

    @Autowired
    private SysConfigMapper sysConfigMapper;

    @Override
    @DataSource(DataSourceType.MASTER)
    public SysConfig selectConfigById(Long configId) {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setConfigId(configId);
       // sysConfigMapper.selectConfig(sysConfig);
        return null;
    }
}
