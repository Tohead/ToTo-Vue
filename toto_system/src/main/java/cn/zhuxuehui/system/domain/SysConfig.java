package cn.zhuxuehui.system.domain;

import cn.zhuxuehui.common.core.domain.BaseEntity;
import org.omg.CORBA.StringHolder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 22:46
 * @description
 */
public class SysConfig extends BaseEntity {

    private static final long serialVersionUID = 2637018387795528887L;

    private Long configId;

    private String configName;

    private String configKey;

    private String configValue;

    private String configType;

    public Long getConfigId() {
        return configId;
    }

    public void setConfigId(Long configId) {
        this.configId = configId;
    }

    @NotBlank(message = "参数名称不能为空")
    @Size(min = 0,max = 100,message = "参数名称不能超过100个字符")
    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    @NotBlank(message = "参数键名长度不能为空")
    @Size(min = 0, max = 100, message = "参数键名长度不能超过100个字符")
    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }


    @NotBlank(message = "参数键值不能为空")
    @Size(min = 0, max = 500, message = "参数键值长度不能超过500个字符")
    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }



    public String getConfigType() {
        return configType;
    }

    public void setConfigType(String configType) {
        this.configType = configType;
    }

    @Override
    public String toString() {
        return "SysConfig{" +
                "configId=" + configId +
                ", configName='" + configName + '\'' +
                ", configkey='" + configKey + '\'' +
                ", configValue='" + configValue + '\'' +
                ", configType='" + configType + '\'' +
                '}';
    }
}
