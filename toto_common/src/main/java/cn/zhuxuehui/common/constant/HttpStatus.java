package cn.zhuxuehui.common.constant;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-14 12:03
 * @description 返回状态码
 */
public class HttpStatus {

    public static final int SUCCESS = 200;

    public static final int CREATED = 201;

    public static final int ACCEPTED = 202;

    public static final int NO_CONTENT = 204;

    public static final int MOVE_PERM = 301;

    public static final int SEE_OTHER = 303;

    public static final int NOT_MODIFIED = 304;

    public static final int BAD_REQUST = 400;

    public static final int UNAUTHORIZED = 401;

    public static final int FORBUDDEN = 403;

    public static final int NOT_FOUND = 404;

    public static final int BAS_METHOD = 405;
    public static final int CONFLICT = 409;

    public static final int UNSUPPORTED_TYPE = 415;

    public static final int ERROR = 500;

    public static final int NOT_IMPLEMENTED = 501;

}
