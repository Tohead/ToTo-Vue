package cn.zhuxuehui.common.enums;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 20:26
 * @description  数据源
 */
public enum DataSourceType {

    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
