package cn.zhuxuehui.common.annotation;

import java.lang.annotation.*;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-10 17:30
 * @description  数据权限过滤注解
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScope {


    /**
     * 部门表的别名
     * @return
     */
    public String deptAlias() default "";


    /**
     * 用户表的别名
     * @return
     */
    public String userAlias() default "";


}
