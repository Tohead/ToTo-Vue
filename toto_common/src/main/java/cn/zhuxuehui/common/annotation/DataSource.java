package cn.zhuxuehui.common.annotation;

import cn.zhuxuehui.common.enums.DataSourceType;

import java.lang.annotation.*;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-14 14:02
 * @description
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {
    public DataSourceType value() default  DataSourceType.MASTER;
}
