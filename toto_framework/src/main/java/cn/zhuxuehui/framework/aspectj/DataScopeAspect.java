package cn.zhuxuehui.framework.aspectj;

import cn.zhuxuehui.common.annotation.DataScope;
import cn.zhuxuehui.common.core.domain.BaseEntity;
import cn.zhuxuehui.common.utils.StringUtils;
import com.fasterxml.jackson.databind.JsonNode;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-10 17:07
 * @description   数据过滤处理
 */
public class DataScopeAspect {

    /**
     * 全部数据权限
     */
    public  static  final  String   DATA_SCOPE_ALL="1";

    /**
     * 自定义数据权限
     */
    public  static  final  String  DATA_SCOPE_CUSTOM="2";

    /**
     * 部门数据权限
     */
    public  static  final  String  DATA_SCOPE_DEPT="3";


    /**
     * 部门及以下数据权限
     */
    public static final String  DATA_SCOPE_DEPT_AND_CHILD="4";

    /**
     * 仅本人数据权限
     */
    public static final String DATA_SCOPE_SELF="5";

    /**
     * 数据权限过滤关键字
     */
    public static final String DATA_SCOPE="dataScope";

    //配置织入点
    @Pointcut("@annotation(cn.zhuxuehui.common.annotation.DataScope)")
    public void dataScopePointCut(){

    }

    @Before("dataScopePointCut()")
    public void  doBefore(JoinPoint point){


    }

    protected void handleDataScope(final JoinPoint joinPoint){
        //获得注解
        DataScope controllerDataScope = getAnnnotationLog(joinPoint);
        if (controllerDataScope==null){
            return;
        }



    }


    /**
     * 是否存在注解，如果存在久获取
     * @param joinPoint
     * @return
     */
    private DataScope getAnnnotationLog(JoinPoint joinPoint){
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature=(MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if (method!=null){
            return method.getAnnotation(DataScope.class);
        }
        return null;
    }

    /**
     * 拼接权限sql 前先清空params.dataScope 参数防止注入
     * @param joinPoint
     */
    public void clearDateScope(final JoinPoint joinPoint){
        Object params = joinPoint.getArgs()[0];

        if(StringUtils.isNotNull(params) && params instanceof BaseEntity){
           BaseEntity baseEntity= (BaseEntity) params;
           baseEntity.getParams().put(DATA_SCOPE,"");

        }

    }

}
