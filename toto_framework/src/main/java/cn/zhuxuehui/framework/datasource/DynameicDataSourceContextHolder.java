package cn.zhuxuehui.framework.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 20:14
 * @description  数据源切换处理
 */

public class DynameicDataSourceContextHolder {

    private static  final Logger log=LoggerFactory.getLogger(DynameicDataSourceContextHolder.class);


    /**
     * 使用ThreadLocal维护变量，ThreadLocal 为每个使用该变量的线程提供独立的变量副本,
     * 所以每一个线程都可以独立地改变自己地副本，而不会影响其他线程对应地副本
     */
    private static final ThreadLocal<String> CONTEXT_HOLDER=new ThreadLocal<>();


    /**
     * 设置数据源地变量
     * @param dsType
     */
    public static void setDataSourceType(String dsType){
        log.info("切换到{}数据源",dsType);
        CONTEXT_HOLDER.set(dsType);
    }


    /**
     * 获取数据源地变量
     * @return
     */
    public static String getDataSourceType(){
        return CONTEXT_HOLDER.get();
    }


    /**
     * 清空数据源变量
     */
    public static void clearDataSourceType(){
        CONTEXT_HOLDER.remove();
    }
}
