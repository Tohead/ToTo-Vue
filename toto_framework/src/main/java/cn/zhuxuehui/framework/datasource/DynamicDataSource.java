package cn.zhuxuehui.framework.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 20:10
 * @description  动态数据源
 */
public class DynamicDataSource extends AbstractRoutingDataSource {


    public DynamicDataSource (DataSource defaultTargetDataSource, Map<Object, Object> targetDataSource){
        super.setDefaultTargetDataSource(defaultTargetDataSource);
        super.setTargetDataSources(targetDataSource);
        super.afterPropertiesSet();
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return DynameicDataSourceContextHolder.getDataSourceType();
    }
}
