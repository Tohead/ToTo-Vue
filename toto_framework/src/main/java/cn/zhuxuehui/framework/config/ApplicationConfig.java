package cn.zhuxuehui.framework.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-14 11:28
 * @description  程序注解配置
 */
@Configuration
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan("cn.zhuxuehui.**.mapper")
public class ApplicationConfig {
}
