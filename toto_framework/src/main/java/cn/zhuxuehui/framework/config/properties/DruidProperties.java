package cn.zhuxuehui.framework.config.properties;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 17:18
 * @description
 */

@Configuration
public class DruidProperties {


    /*初始连接数*/
    @Value("${spring.datasource.druid.initialSize}")
    private int initialSize;


    /*最小连接池数量*/
    @Value("${spring.datasource.druid.minIdle}")
    private int minIdle;

    /*最大连接数量*/
    @Value("${spring.datasource.druid.maxActive}")
    private int maxActive;


    /*连接等待超时时间*/
    @Value("${spring.datasource.druid.maxWait}")
    private int maxWait;

    /*配置间隔多久才进行一次检测，检测需要关闭的空闲连接,单位是毫秒*/
    @Value("${spring.datasource.druid.timeBetweenEvictionRunsMillis}")
    private int timeBetweenEvictionRunsMillis;

    /*一个连接在池中最小生存的时间，单位是毫秒*/
    @Value("${spring.datasource.druid.minEvictableIdleTimeMillis}")
    private int minEvictableIdleTimeMillis;

    /*一个连接在池中最大的生存时间*/
    @Value("${spring.datasource.druid.maxEvictableIdleTimeMillis}")
    private int maxEvictableIdleTimeMillis;

    /*配置检测连接是否有效*/
    @Value("${spring.datasource.druid.validationQuery}")
    private String validationQuery;

    /**/
    @Value("${spring.datasource.druid.testWhileIdle}")
    private boolean testWhileIdle;
    /**/
    @Value("${spring.datasource.druid.testOnBorrow}")
    private boolean testOnBorrow;
    /**/
    @Value("${spring.datasource.druid.testOnReturn}")
    private boolean testOnReturn;



    public DruidDataSource dataSource(DruidDataSource datasource){

        /*配置初始化大小、最小 、最大*/
        datasource.setInitialSize(initialSize);
        datasource.setMaxActive(maxActive);
        datasource.setMinIdle(minIdle);

        /*配置获取连接等待超时时间*/
        datasource.setMaxWait(maxWait);

        /*配置间隔多久才进行一次检测，检测需要关闭的空闲连接,单位是毫秒*/
        datasource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);

        /*配置一个连接在池中最小、最大生存的时间，单位是毫秒*/
        datasource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        datasource.setMaxEvictableIdleTimeMillis(maxEvictableIdleTimeMillis);


        /*用来检测连接是否有效的sql 要求是一个查询语句，
        *  常用select 'x'。 如果validationQuery 为空
        *  testOnBorrow testOnReturn  testWhileIdle 都不会起作用
        */
        datasource.setValidationQuery(validationQuery);

        /*
            建议配置为true 不影响性能，并保证安全性。申请连接得时候检测，如果空闲时间大于timeBetweenEvicetionMillis,执行validationQuery
            检测连接是否有效
         */
        datasource.setTestWhileIdle(testWhileIdle);

        /*申请连接时执行validationQuery 检测连接是否有效，做了这个配置会降低性能*/
        datasource.setTestOnBorrow(testOnBorrow);

        /*归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能*/
        datasource.setTestOnReturn(testOnReturn);
        return datasource;
    }
}
