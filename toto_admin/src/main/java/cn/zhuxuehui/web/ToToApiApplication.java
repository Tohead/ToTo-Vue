package cn.zhuxuehui.web;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author toto
 * @version 1.0
 * @date 2021-07-13 16:59
 * @description
 */
//@ComponentScan(basePackages = {"cn.zhuxuehui"})
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class
       , DruidDataSourceAutoConfigure.class, HibernateJpaAutoConfiguration.class})
//@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class ToToApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ToToApiApplication.class,args);
    }
}
